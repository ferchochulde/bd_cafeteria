/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cafeteria.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jferc
 */
@Entity
@Table(name = "VALOR_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValorProducto.findAll", query = "SELECT v FROM ValorProducto v"),
    @NamedQuery(name = "ValorProducto.findByIdValor", query = "SELECT v FROM ValorProducto v WHERE v.idValor = :idValor"),
    @NamedQuery(name = "ValorProducto.findByValor", query = "SELECT v FROM ValorProducto v WHERE v.valor = :valor"),
    @NamedQuery(name = "ValorProducto.findByIva", query = "SELECT v FROM ValorProducto v WHERE v.iva = :iva"),
    @NamedQuery(name = "ValorProducto.findByDescuento", query = "SELECT v FROM ValorProducto v WHERE v.descuento = :descuento")})
public class ValorProducto implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_VALOR", nullable = false, precision = 0, scale = -127)
    private BigDecimal idValor;
    @Basic(optional = false)
    @Column(name = "VALOR", nullable = false, precision = 10, scale = 2)
    private BigDecimal valor;
    @Basic(optional = false)
    @Column(name = "IVA", nullable = false, precision = 10, scale = 2)
    private BigDecimal iva;
    @Basic(optional = false)
    @Column(name = "DESCUENTO", nullable = false, precision = 10, scale = 2)
    private BigDecimal descuento;
    @OneToMany(mappedBy = "idValor")
    private List<Producto> productoList;

    public ValorProducto() {
    }

    public ValorProducto(BigDecimal idValor) {
        this.idValor = idValor;
    }

    public ValorProducto(BigDecimal idValor, BigDecimal valor, BigDecimal iva, BigDecimal descuento) {
        this.idValor = idValor;
        this.valor = valor;
        this.iva = iva;
        this.descuento = descuento;
    }

    public BigDecimal getIdValor() {
        return idValor;
    }

    public void setIdValor(BigDecimal idValor) {
        this.idValor = idValor;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    @XmlTransient
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idValor != null ? idValor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorProducto)) {
            return false;
        }
        ValorProducto other = (ValorProducto) object;
        if ((this.idValor == null && other.idValor != null) || (this.idValor != null && !this.idValor.equals(other.idValor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cafeteria.model.entities.ValorProducto[ idValor=" + idValor + " ]";
    }
    
}
