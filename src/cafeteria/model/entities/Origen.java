/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cafeteria.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jferc
 */
@Entity
@Table(name = "ORIGEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Origen.findAll", query = "SELECT o FROM Origen o"),
    @NamedQuery(name = "Origen.findByIdOrigen", query = "SELECT o FROM Origen o WHERE o.idOrigen = :idOrigen"),
    @NamedQuery(name = "Origen.findByPaisOrigen", query = "SELECT o FROM Origen o WHERE o.paisOrigen = :paisOrigen")})
public class Origen implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_ORIGEN", nullable = false, precision = 0, scale = -127)
    private BigDecimal idOrigen;
    @Basic(optional = false)
    @Column(name = "PAIS_ORIGEN", nullable = false, length = 50)
    private String paisOrigen;
    @OneToMany(mappedBy = "idOrigen")
    private List<Producto> productoList;

    public Origen() {
    }

    public Origen(BigDecimal idOrigen) {
        this.idOrigen = idOrigen;
    }

    public Origen(BigDecimal idOrigen, String paisOrigen) {
        this.idOrigen = idOrigen;
        this.paisOrigen = paisOrigen;
    }

    public BigDecimal getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(BigDecimal idOrigen) {
        this.idOrigen = idOrigen;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    @XmlTransient
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrigen != null ? idOrigen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Origen)) {
            return false;
        }
        Origen other = (Origen) object;
        if ((this.idOrigen == null && other.idOrigen != null) || (this.idOrigen != null && !this.idOrigen.equals(other.idOrigen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cafeteria.model.entities.Origen[ idOrigen=" + idOrigen + " ]";
    }
    
}
