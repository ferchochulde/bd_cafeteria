/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cafeteria.model.manager;

import cafeteria.model.entities.Origen;
import cafeteria.model.entities.Producto;
import cafeteria.model.entities.ValorProducto;
import java.awt.TextArea;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jferc
 */
public class ManagerPrecio {
    EntityManager em ;
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("cafeteriaPU");
    
    
        public void crearPrecio(int id, double valor, String iva, String descuento){
            em = emf.createEntityManager();
            em.getTransaction().begin();
            
            ValorProducto valorProducto = new ValorProducto();
            
            valorProducto.setIdValor(BigDecimal.valueOf(id));
            valorProducto.setValor(BigDecimal.valueOf(valor));
            valorProducto.setIva(BigDecimal.valueOf(Double.parseDouble(iva)));
            valorProducto.setDescuento(BigDecimal.valueOf(Double.parseDouble(descuento)));
            
            em.persist(valorProducto);  //me permite ingresar datos constantemente
            em.getTransaction().commit();
        }
        
        public Origen findOrigenById(int id){
            BigDecimal id_or=BigDecimal.valueOf(id);
            return em.find(Origen.class, id_or);
        }
       
        
        public ValorProducto findValorById(int id){
            BigDecimal id_val=BigDecimal.valueOf(id);
            return em.find(ValorProducto.class, id_val);
        }
        
        public void crearProducto(int id, String nombre, String descripcion, int idPais, int idValor) {
            
            
            Origen origen = this.findOrigenById(idPais);
            ValorProducto valor = this.findValorById(idValor);
            //consutla find by id
            
             em = emf.createEntityManager();
            em.getTransaction().begin();
            
            Producto producto = new Producto();
            
            producto.setIdProducto(BigDecimal.valueOf(id));
            producto.setNombre(nombre);
            producto.setDescripcion(descripcion);
            producto.setIdOrigen(origen);
            producto.setIdValor(valor);
            
            em.persist(producto);
            em.getTransaction().commit();
        }
        
        public void eliminarProducto(int idProducto, int idPrecio) {
             em = emf.createEntityManager();
        em.getTransaction().begin();
        
        
        ValorProducto pre = em.find(ValorProducto.class, BigDecimal.valueOf(idProducto));
            em.remove(pre); 
        Producto p = em.find(Producto.class, BigDecimal.valueOf(idProducto));
            em.remove(p); // mando a borrar el jpa cuya clave primaria sea el texto
            em.getTransaction().commit();    
        }
        
        public void listarTabla(DefaultTableModel model, List<ValorProducto> listaValProd, List<Producto> listaProductos){
            model.setRowCount(0);
            
            for (int i = 0; i < listaValProd.size(); i++)
            {
                double valor = Double.parseDouble(listaProductos.get(i).getIdValor().getValor().toString());
                double iva = Double.parseDouble(listaProductos.get(i).getIdValor().getIva().toString());
                iva = iva / 100;
                double total = valor + (valor*iva);
                
                model.addRow(new Object[] {
                    listaProductos.get(i).getIdProducto(),
                    listaProductos.get(i).getNombre(),
                    listaProductos.get(i).getDescripcion(),
                    listaProductos.get(i).getIdOrigen().getPaisOrigen(),
                    listaProductos.get(i).getIdValor().getValor(),
                    listaProductos.get(i).getIdValor().getIva(),
                    valor*iva,
                    total,
                    listaProductos.get(i).getIdValor().getDescuento(),
                    String.format ("%.2f", total-Double.parseDouble(listaProductos.get(i).getIdValor().getDescuento().toString()))
                    
               });
            }
        }
        
        
        public List<ValorProducto> consultarPrecios(){
        em = emf.createEntityManager();
        em.getTransaction().begin();

        Query q = em.createQuery("SELECT v FROM ValorProducto v", Origen.class);  
        List<ValorProducto> lista = q.getResultList(); 
        em.getTransaction().commit();
        return lista;
        }
        
         public List<Producto> consultarProductos(){
        em = emf.createEntityManager();
        em.getTransaction().begin();

        Query q = em.createQuery("SELECT p FROM Producto p", Producto.class);  
        List<Producto> lista = q.getResultList(); 
        em.getTransaction().commit();
        return lista;
        }
         
         
         public void consultarPais(JComboBox cmbOrigen){
             ManagerOrigen mOrigen=new ManagerOrigen();
             List<Origen> listaorigen;
             listaorigen=mOrigen.consultarPais();
    
             String[] p=new String[listaorigen.size()];
             for (int i = 0; i < listaorigen.size(); i++) {
                p[i]=listaorigen.get(i).getPaisOrigen();
            }
            cmbOrigen.setModel(new javax.swing.DefaultComboBoxModel<>(p));
         
         }
         
        
         
         
         
            
    
}
