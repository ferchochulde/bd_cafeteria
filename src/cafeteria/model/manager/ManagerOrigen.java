/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cafeteria.model.manager;

import cafeteria.model.entities.Origen;
import cafeteria.model.entities.Producto;
import java.awt.TextArea;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jferc
 */
public class ManagerOrigen {
    EntityManager em ;
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("cafeteriaPU");
    
       
    
        public void crearProducto(String nombre,String descripcion){
            em = emf.createEntityManager();
            em.getTransaction().begin();
            
            Producto p=new Producto();
            
            p.setNombre(nombre);
            p.setDescripcion(descripcion);
           
            em.persist(p);  //me permite ingresar datos constantemente
            em.getTransaction().commit();
        }
        
        public void crearOrigen(String pais, int id){
            
            em = emf.createEntityManager();
            em.getTransaction().begin();
            
            Origen o=new Origen();
            
            o.setPaisOrigen(pais);
            o.setIdOrigen(BigDecimal.valueOf(id));
            
            em.persist(o);  //me permite ingresar datos constantemente
            em.getTransaction().commit();
        }
        
        public void listarTabla(DefaultTableModel model, List<Origen> listaPaises){
            model.setRowCount(0);
            
            for (int i = 0; i < listaPaises.size(); i++)
            {
                model.addRow(new Object[] {
                    listaPaises.get(i).getIdOrigen(),
                    listaPaises.get(i).getPaisOrigen(),
               });
            }
        }
        
        
        public List<Origen> consultarPais(){
        em = emf.createEntityManager();
        em.getTransaction().begin();

        Query q = em.createQuery("SELECT o FROM Origen o", Origen.class); //se manejan objetos 
        List<Origen> lista = q.getResultList();   //Lista de objetos mensajes

        em.getTransaction().commit();
        
        return lista;
        }
        
        public void eliminarPais(int idPais){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        Origen in = em.find(Origen.class, BigDecimal.valueOf(idPais));
            em.remove(in); // mando a borrar el jpa cuya clave primaria sea el texto
            em.getTransaction().commit();      
        }
        
        public void editarPais(int idPais){
            em = emf.createEntityManager();
            em.getTransaction().begin();
            Origen o = em.find(Origen.class, BigDecimal.valueOf(idPais));
            o.setPaisOrigen(JOptionPane.showInputDialog("Ingrese pais: "));
            em.merge(o);
            em.getTransaction().commit();
        }
            
        
           public Origen FindByOrigen(String nombreOrigen) {
    	Origen origen = new Origen();
    	List<Origen> listadeUsuarios = this.consultarPais();
    	for (int i = 0; i < listadeUsuarios.size(); i++) {
    		if (listadeUsuarios.get(i).getPaisOrigen().equals(nombreOrigen)) {
				origen = listadeUsuarios.get(i);
				break;
			}
    	}
    	return origen;
    }
    
}
